﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;

namespace Kontaktbuch
{
    class MainViewModel : ModelBase
    {
        Contact viewContact;
        List<Contact> contacts = new List<Contact>();

        MainWindow window;

        public string firstName { get => viewContact.firstName; set { viewContact.firstName = value; OnPropertyChanged("firstName"); } }
        public string lastName { get => viewContact.lastName; set { viewContact.lastName = value; OnPropertyChanged("lastName"); } }
        public string postcode { get => viewContact.adress.postcode; set { viewContact.adress.postcode = value; OnPropertyChanged("postcode"); } }
        public string location { get => viewContact.adress.location; set { viewContact.adress.location = value; OnPropertyChanged("location"); } }


        public MainViewModel(MainWindow window)
        {
            viewContact = new Contact("Max", "Mustermann", new Adress("6314", "Unteraegeri"));
            this.window = window;

            this.window.DataContext = this;
            hiButtonCommand = new CommandHandler(cleanInputAndAddToList);
        }

        public ICommand hiButtonCommand { get; set; }

        public void cleanInputAndAddToList()
        {
            contacts.Add(viewContact);
            viewContact = new Contact("", "", new Adress("", ""));

            // Refresh input data
            OnPropertyChanged("firstName");
            OnPropertyChanged("lastName");
            OnPropertyChanged("postcode");
            OnPropertyChanged("location");
        }
    }

    public class CommandHandler : ICommand
    {
        private Action _action;

        public CommandHandler(Action action)
        {
            _action = action;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            _action();
        }
    }

    public class ModelBase : INotifyPropertyChanged
        {
            public event PropertyChangedEventHandler PropertyChanged;

            public void OnPropertyChanged(string propertyName)
            {
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }

        }

}
