﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Kontaktbuch
{
    class Contact
    {
        public string firstName;
        public string lastName;
        public Adress adress;

        public Contact(string firstName, string lastName, string postcode, string location)
        {
            this.firstName = firstName;
            this.lastName = lastName;
            this.adress = new Adress(postcode, location);
        }
        public Contact(string firstName, string lastName, Adress adress)
        {
            this.firstName = firstName;
            this.lastName = lastName;
            this.adress = adress;
        }
    }
    class Adress
    {
        public string postcode;
        public string location;

        public Adress(string postcode, string location)
        {
            this.postcode = postcode;
            this.location = location;
        }
    }

}
